import numpy as np
from keras.models import model_from_json
import utils

def load_data():
    # load data and labels from file
    print("loading data and labels")
    data_ = np.load("data/dataGlove300ALL.npy")
    labels_ = np.load("data/labelsGlove300ALL.npy")

    return data_, labels_


def split_data(data_, labels_):
    # split the data into a training set and a validation set
    indices = np.arange(data_.shape[0])
    np.random.shuffle(indices)
    data_ = data_[indices]
    labels_ = labels_[indices]

    num_validation_samples = int(VALIDATION_SPLIT * data_.shape[0])

    x_train = data_[:-num_validation_samples]
    y_train = labels_[:-num_validation_samples]
    x_val = data_[-num_validation_samples:]
    y_val = labels_[-num_validation_samples:]

    return x_train, y_train, x_val, y_val


def load_model():
    # load json and create model
    json_file = open('data/modelCNN_r2g.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model_ = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model_.load_weights("data/modelCNN_r2g.h5")
    print("Loaded model from file")

    return loaded_model_


def train_model(loaded_model_, x_train_, y_train_, x_val_, y_val_):
    #train model
    loaded_model_.compile(loss='categorical_crossentropy',
                       optimizer='rmsprop',
                       metrics=['acc'])

    epochRecall = []
    isImproved = []

    for epoch in range(EPOCHS):
        loaded_model_.fit(x_train_, y_train_,
                       batch_size=BATCH_SIZE,
                       epochs=1,
                       validation_data=(x_val_, y_val_))

        [recall, recall_p, recall_u, recall_n] = utils.avg_recall_on_training_end(loaded_model_, x_val_, y_val_)
        print("Average recall on Epoch " + str(epoch + 1) + "/" + str(EPOCHS) + ": " + str(recall))

        #Early stopping criteria
        if epoch == 0:
            epochRecall.append(recall)
            isImproved.append(False)
        else:
            if recall >= max(epochRecall) + MIN_IMPROVEMENT:
                improvement = True
            else:
                improvement = False

            epochRecall.append(recall)
            isImproved.append(improvement)


            if epoch >= IMPROVEMENT_PATIENCE:
                if True in isImproved[epoch - IMPROVEMENT_PATIENCE:epoch+1]:  # to check the current epoch also
                    continue
                else:
                    print("Early stopping criteria met")
                    print("Training stopped")
                    break

    print("Max recall: %s, found in epoch %s" % (max(epochRecall), epochRecall.index(max(epochRecall)) + 1))

    print("Model trained")

    utils.log_to_json(max(epochRecall), epochRecall.index(max(epochRecall)) + 1, JSON_FILE)

    return epochRecall


if __name__ == "__main__":

    np.random.seed(9999)  # for reproducibility

    #####PARAMS OF THE SAVED MODEL#####
    # MAX_SEQUENCE_LENGTH = 50
    # MAX_NB_WORDS = 20000
    # EMBEDDING_DIM = 300
    # NUMBER_OF_FILTERS = 128
    # FILTER_KERNEL_SIZE = 5
    # MAX_POOLING_WINDOW = 5
    # EMBEDDING_TOOL = "GloVe"

    MIN_IMPROVEMENT = 0.001
    IMPROVEMENT_PATIENCE = 20

    VALIDATION_SPLIT = 0.2
    EPOCHS = 1
    BATCH_SIZE = 11000

    JSON_FILE = "/output/runs.json"

    [data, labels] = load_data()

    [x_train, y_train, x_val, y_val] = split_data(data, labels)

    loaded_model = load_model()

    _ = train_model(loaded_model, x_train, y_train, x_val, y_val)

    # list_of_recalls = train_model(loaded_model)








