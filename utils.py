import os
import json

def class_recall(pred_classes, true_classes, class_label):

    if class_label == "positive":
        class_id = 0
    elif class_label == "neutral":
        class_id = 1
    elif class_label == "negative":
        class_id = 2
    else:
        print("error in classes")

    tp = 0 # true positive
    for i in range(len(pred_classes)):
        if pred_classes[i] == true_classes[i]:
            if pred_classes[i] == class_id:
                tp += 1

    return tp/(true_classes.count(class_id))


def avg_recall_on_training_end(model, x_val, y_val):
        output = model.predict(x_val)
        pred_classes = []
        true_classes = []



        for i in range(x_val.shape[0]):
            pred_classes.append(output[i].argmax())
            true_classes.append(y_val[i].argmax())

        #douleuei kai me auto!! alla ithela na vlepo ta epimerouw recalls ton classeon
        # print("sklearn.metrics: recall_score = " + str(recall_score(true_classes, pred_classes, average='macro')))

        recall_p = class_recall(pred_classes, true_classes, class_label="positive")
        recall_u = class_recall(pred_classes, true_classes, class_label="neutral")
        recall_n = class_recall(pred_classes, true_classes, class_label="negative")

        print("Positive recall: " + str(recall_p))
        print("Neutral recall: " + str(recall_u))
        print("Negative recall: " + str(recall_n))

        avg_rec = (recall_p + recall_u + recall_n) / 3

        return avg_rec, recall_p, recall_u, recall_n


def log_to_json(recall, epoch, file):

    if not os.path.isfile(file):
        open(file, 'a').close()

    with open(file, mode='r', encoding='utf-8') as feedsjson:
        try:
            feeds = json.load(feedsjson)

        except:
            feeds = []

    with open(file, mode='w', encoding='utf-8') as feedsjson:
        entry = {'Recall': recall,
                 'Epochs': epoch}
        feeds.append(entry)
        json.dump(feeds, feedsjson, sort_keys=True, indent=2)

    print("Logged to file")

